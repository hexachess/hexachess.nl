import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { first } from 'rxjs/operators';
import { AuthService } from './core/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(
    private store: Store,
    private authService: AuthService,
    private router: Router
  ) {
    router.events.subscribe(() => {
      this.username = '';
      this.store
        .select((state) => state.users.users)
        .pipe(first())
        .subscribe((result) => {
          if (result != undefined) {
            if (result[0] != undefined) {
              this.username = result[0].username;
            }
          }
        }, console.log);
      this.isLoggedIn = this.username != '';
    });
  }

  title = 'hexachess';
  isLoggedIn = false;
  username = '';

  ngOnInit(): void {
    this.store
      .select((state) => state.users.users)
      .pipe(first())
      .subscribe((result) => {
        if (result != undefined) {
          if (result[0] != undefined){
            this.username = result[0].username;
          }
        }
      }, console.log);
    this.isLoggedIn = this.username != '';
  }

  logOut() {
    this.authService.logOut().subscribe(console.log, console.log);
  }
}
