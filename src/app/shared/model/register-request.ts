export class RegisterRequest {
  constructor(private username: string, private email: string, private password: string, private mfa: boolean) {}
}
