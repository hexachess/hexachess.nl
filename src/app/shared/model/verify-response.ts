import { Token } from "./token";

export class VerifyCodeResponse {
  constructor(public token: Token, public image: string, public secret: string) {}
}
