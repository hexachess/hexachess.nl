import { Game } from "./game";
import { User } from "./user";

export class Move {
  constructor(public player: User, public game: Game, public oldX: number, public oldY: number, public newX: number, public newY: number) {}
}
