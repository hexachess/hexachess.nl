import { Move } from "./move";
import { User } from "./user";

export class Game {
  constructor(public id: number, public mode: number, public dateStart: number, public dateEnd: number, public moves: Move[], public players: User[], public winner: string) {}
}
