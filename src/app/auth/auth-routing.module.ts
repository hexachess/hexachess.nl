import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component'
import { AuthGuardService } from '../core/services/auth-guard.service';
import { RegisterComponent } from './components/register/register.component';
import { UserGuardService } from '../core/services/user-guard.service';
import { VerifyComponent } from './components/verify/verify.component';

const routes: Routes = [
  { path: 'auth/profile', component: ProfileComponent, canActivate: [AuthGuardService] },
  { path: 'auth/login', component: LoginComponent, canActivate: [UserGuardService] },
  { path: 'auth/register', component: RegisterComponent, canActivate: [UserGuardService]},
  { path: 'auth/verify', component: VerifyComponent, canActivate: [UserGuardService]},
  { path: '', redirectTo: 'auth/profile', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
