import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth.service';
import { User } from 'src/app/shared/model/user';
import { VerifyCodeRequest } from 'src/app/shared/model/verify-request';
import { VerifyCodeResponse } from 'src/app/shared/model/verify-response';
import { AddUser } from 'src/app/shared/state/action';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss'],
})
export class VerifyComponent implements OnInit {
  @Input() code = '';
  qrCode = '';
  secret = '';

  private verifyCodeResponse!: VerifyCodeResponse;

  constructor(
    private router: Router,
    private authService: AuthService,
    private toastrService: ToastrService,
    private store: Store
  ) {
    if (this.router.getCurrentNavigation()?.extras.state != null) {
      this.verifyCodeResponse = this.router.getCurrentNavigation()?.extras.state?.verifyCodeResponse;
      this.qrCode = this.verifyCodeResponse.image;
      this.secret = this.verifyCodeResponse.secret;
    }
  }

  ngOnInit(): void {}

  onSubmit() {
    let request: VerifyCodeRequest = new VerifyCodeRequest(
      this.code,
      this.verifyCodeResponse.token.tokenValue
    );
    let user: User;
    this.authService.verify(request).subscribe((response: Response) => {
      if (response.status == 200) {
        this.authService.getUser().subscribe(
          (response: User) => (user = response),
          (error) => console.log(error),
          () => {
            this.store.dispatch(new AddUser(user));
            this.router.navigate(['/auth/profile']);
          }
        );
      } else if (response.status == 100) {
        console.log(response);
      } else {
        this.toastrService.error('Invalid credentials');
      }
    }, console.log);
  }
}
