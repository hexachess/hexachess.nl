import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../../../core/services/auth.service';
import { LoginRequest } from '../../../shared/model/login-request';
import { User } from '../../../shared/model/user';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { ToastrService } from 'ngx-toastr';
import { HttpResponse } from '@angular/common/http';
import { AddUser, Reset } from 'src/app/shared/state/action';
import { VerifyCodeResponse } from 'src/app/shared/model/verify-response';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(
    private aut: AuthService,
    private toastr: ToastrService,
    private store: Store,
    private router: Router
  ) {}

  @Input() username = '';
  @Input() password = '';

  ngOnInit(): void {
  }

  onSubmit() {
    let request: LoginRequest = new LoginRequest(this.username, this.password);
    let user: User;
    this.aut.signIn(request).subscribe(
      (response: Response) => {
        console.log(response);
        if (response.status == 200) {
          this.aut.getUser().subscribe(
            (response: User) => (user = response),
            (error) => console.log(error),
            () => {
              this.store.dispatch(new AddUser(user));
              this.router.navigate(['/auth/profile']);
            }
          );
        } else {
          this.toastr.error('Invalid credentials');
        }
      },      
      (error) => {
        if (error.status == 402) {
          let verifyCodeResponse: VerifyCodeResponse;
          verifyCodeResponse = error.error;
          this.router.navigateByUrl('/auth/verify', {
            state: { verifyCodeResponse: verifyCodeResponse }
          });
        }
        else{
          console.log(error);
        }
      }
    );
  }
}
