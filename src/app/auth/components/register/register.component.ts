import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../../../core/services/auth.service';
import { LoginRequest } from '../../../shared/model/login-request';
import { User } from '../../../shared/model/user';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { ToastrService } from 'ngx-toastr';
import { HttpResponse } from '@angular/common/http';
import { AddUser } from 'src/app/shared/state/action';
import { RegisterRequest } from 'src/app/shared/model/register-request';
import { VerifyCodeResponse } from 'src/app/shared/model/verify-response';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private toastrService: ToastrService,
    private store: Store,
    private router: Router
  ) {}

  @Input() username = '';
  @Input() email = '';
  @Input() password = '';
  @Input() passwordConfirmation = '';
  @Input() twoFactorAuthentication = true;

  ngOnInit(): void {}

  onSubmit() {
    if (this.password !== this.passwordConfirmation) {
      this.toastrService.error('Passwords do not match');
    }
    let request: RegisterRequest = new RegisterRequest(
      this.username,
      this.email,
      this.password,
      this.twoFactorAuthentication
    );
    console.log(request);
    let user: User;
    this.authService.register(request).subscribe(
      (response: Response) => {
        if (response.status == 200) {
          this.authService.getUser().subscribe(
            (response: User) => (user = response),
            (error) => console.log(error),
            () => {
              this.store.dispatch(new AddUser(user));
              this.router.navigate(['/auth/profile']);
            }
          );
        } else if (response.status == 100) {
          console.log(response);
        } else {
          this.toastrService.error('Invalid credentials');
        }
      },
      (error) => {
        if (error.status == 402) {
          let verifyCodeResponse: VerifyCodeResponse;
          verifyCodeResponse = error.error;
          this.router.navigateByUrl('/auth/verify', {
            state: { verifyCodeResponse: verifyCodeResponse }
          });
        }
        else{
          console.log(error);
        }
      }
    );
  }
}
