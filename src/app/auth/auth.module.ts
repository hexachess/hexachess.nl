import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth-routing.module';
import { NgxsModule } from '@ngxs/store';
import { UserState } from '../shared/state/state';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { VerifyComponent } from './components/verify/verify.component';



@NgModule({
  declarations: [LoginComponent, RegisterComponent, ProfileComponent, VerifyComponent],
  imports: [
    CommonModule,
    FormsModule,
    AuthRoutingModule,
    NgxsModule.forFeature(
      [UserState]
    ),
    NgxsStoragePluginModule.forRoot(
      {key: UserState}
    ),
    NgxsLoggerPluginModule.forRoot()
  ],
  providers: []
})
export class AuthModule { }
