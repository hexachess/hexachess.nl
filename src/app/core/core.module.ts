import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxsModule } from '@ngxs/store';
import { UserState } from '../shared/state/state';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgxsModule.forRoot(
      [UserState]
    ),
    NgxsStoragePluginModule.forRoot(
      {key: UserState}
    ),
    NgxsLoggerPluginModule.forRoot()
  ],
  providers: []
})
export class CoreModule { }
