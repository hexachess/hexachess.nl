import { HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { ToastrService } from 'ngx-toastr';

// import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private toaster: ToastrService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const re1 = '/assets';
    const re2 = '/auth/logout';
    const re3 = '/auth/login';
    if (
      request.url.search(re1) === -1 &&
      request.url.search(re2) === -1 &&
      request.url.search(re3) === -1
    ) {
      request = request.clone({
        withCredentials: true,
      });
    }
    return next.handle(request).pipe(
      catchError((err: any) => {
        console.log(err);
        if (err.status == 401) {
          if (err.headers.get('Message') == 'Invalid token') {
            console.log(2);
            return this.refreshToken(request, next);
          } else {
            console.log(3);
            this.toaster.info('Session expired');
            this.authService.logOut().subscribe(
              (res) => console.log(res),
              (err) => console.log(err)
            );
          }
        }
        return throwError(err);
      })
    );
  }
  private refreshToken(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return this.authService.refresh().pipe(
      switchMap((res) => {
        return next.handle(this.addAuthorizationHeader(request, res.status));
      })
    );
  }
  private addAuthorizationHeader(
    request: HttpRequest<any>,
    res: string
  ): HttpRequest<any> {
    if (res.toString() == 'SUCCESS') {
      // if refresh token is valid
      return request.clone({ withCredentials: true });
    }
    // if refresh token is invalid
    this.authService.logOut().subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );
    throw("Session expired");
  }
}

export const authInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
];