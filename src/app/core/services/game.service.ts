import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Game } from 'src/app/shared/model/game';
import { Move } from 'src/app/shared/model/move';
import { environment } from 'src/environments/environment';
import { EventService } from './event.service';

const URL_BASE = environment.baseUrl;

@Injectable({
  providedIn: 'root',
})
export class GameService {
  constructor(
    private router: Router,
    private store: Store,
    private httpClient: HttpClient,
    private eventService: EventService
  ) {}
  createGame(request: Game): Observable<Game> {
    return this.httpClient.post<any>(URL_BASE + '/game', request, {
      withCredentials: true,
    });
  }
  getGame(id: number): Observable<Game> {
    return this.httpClient.get<any>(URL_BASE + '/game/' + id, {
      withCredentials: true,
    });
  }
  removeGame(id: number): Observable<any> {
    return this.httpClient.delete<any>(URL_BASE + '/game/' + id, {
      withCredentials: true,
    });
  }
  getGames(username: string): Observable<Game[]> {
    return this.httpClient.get<any>(URL_BASE + '/game/overview/' + username, {
      withCredentials: true,
    });
  }
  joinGame(id: number): Observable<Game> {
    return this.httpClient.get<any>(URL_BASE + '/game/join/' + id, {
      withCredentials: true,
    });
  }
  placeMove(request: Move): Observable<any> {
    return this.httpClient.post<any>(
      URL_BASE + '/game/' + request.game.id,
      request,
      {
        withCredentials: true,
        observe: 'response',
      }
    );
  }
}
