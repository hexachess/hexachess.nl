import { Injectable } from '@angular/core';
import { LoginRequest } from 'src/app/shared/model/login-request';
import { Router } from '@angular/router';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Store } from '@ngxs/store';
import { Reset } from 'src/app/shared/state/action';
import { User } from 'src/app/shared/model/user';
import { RegisterRequest } from 'src/app/shared/model/register-request';
import { VerifyCodeRequest } from 'src/app/shared/model/verify-request';

const URL_BASE = environment.baseUrl;

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private router: Router,
    private store: Store,
    private httpClient: HttpClient
  ) {}
  signIn(request: LoginRequest): Observable<any> {
    return this.httpClient.post<any>(URL_BASE + '/auth/signin', request, {
      withCredentials: true,
      observe: 'response',
    });
  }
  register(request: RegisterRequest): Observable<any> {
    return this.httpClient.post<any>(URL_BASE + '/auth/signup', request, {
      withCredentials: true,
      observe: 'response',
    });
  }
  verify(request: VerifyCodeRequest): Observable<any> {
    return this.httpClient.post<any>(URL_BASE + '/auth/verify', request, {
      withCredentials: true,
      observe: 'response',
    });
  }
  refresh(): Observable<any> {
    return this.httpClient.post<any>(URL_BASE + '/auth/refresh', {
      withCredentials: true,
    });
  }
  getUser(): Observable<any> {
    return this.httpClient
      .get<any>(URL_BASE + '/user', { withCredentials: true })
      .pipe(
        tap((user: User) => {
          console.log(user);
        })
      );
  }
  logOut(): Observable<any> {
    return this.httpClient
      .get(URL_BASE + '/auth/signout', { withCredentials: true })
      .pipe(
        tap(
          () => {
            this.store.dispatch(new Reset());
            localStorage.clear();
            this.router.navigate(['auth/login']);
          },
          () => {
            this.store.dispatch(new Reset());
            localStorage.clear();
            this.router.navigate(['auth/login']);
          }
        )
      );
  }
}
