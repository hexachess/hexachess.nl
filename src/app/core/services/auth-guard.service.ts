import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { Store } from '@ngxs/store';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate {
  constructor(private store: Store, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    let isLoggedIn = false;
    this.store
      .select((state) => state.users.users)
      .pipe(first())
      .subscribe((result) => {
        if (result != undefined) {
          if (result[0] != undefined){
            isLoggedIn = result.length > 0;
          }
        }
      }, console.log);
    return isLoggedIn;
  }
}
