import { Injectable, NgZone } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { environment } from 'src/environments/environment';

const URL_BASE = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private _zone: NgZone){}

  private fetchGameMoveUpdates(id: number): EventSource {
    return new EventSource(URL_BASE + "/game/moveupdates/" + id);
  }

  getGameMoveUpdates(id: number){
    return new Observable((observer: Observer<any>) => {
      const eventSource = this.fetchGameMoveUpdates(id);

      eventSource.onmessage = event => {
        this._zone.run(() =>{
          observer.next(event.data);
        });
      }

      eventSource.onerror = error => {
        this._zone.run(() =>{
          observer.error(error);
        });
      }
    })
  }

  private fetchGameJoinUpdates(id: number): EventSource {
    return new EventSource(URL_BASE + "/game/joinupdates/" + id);
  }

  getGameJoinUpdates(id: number){
    return new Observable((observer: Observer<any>) => {
      const eventSource = this.fetchGameJoinUpdates(id);

      eventSource.onmessage = event => {
        this._zone.run(() =>{
          observer.next(event.data);
        });
      }

      eventSource.onerror = error => {
        this._zone.run(() =>{
          observer.error(error);
        });
      }
    })
  }
}
