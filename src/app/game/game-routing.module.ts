import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../core/services/auth-guard.service';
import { GameComponent } from './components/game/game.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NewGameComponent } from './components/new-game/new-game.component';
import { JoinGameComponent } from './components/join-game/join-game.component';

const routes: Routes = [
  { path: 'game/id/:id', component: GameComponent, canActivate: [AuthGuardService] },
  { path: 'game/join/:id', component: JoinGameComponent, canActivate: [AuthGuardService] },
  { path: 'game/overview/:username', component: DashboardComponent, canActivate: [AuthGuardService] },
  { path: 'game/overview', component: DashboardComponent, canActivate: [AuthGuardService]},
  { path: 'game/new', component: NewGameComponent, canActivate: [AuthGuardService]},
  { path: '', redirectTo: 'game/overview', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GameRoutingModule { }
