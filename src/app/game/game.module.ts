import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { GameComponent } from './components/game/game.component';
import { FormsModule } from '@angular/forms';
import { NgxsModule } from '@ngxs/store';
import { UserState } from '../shared/state/state';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { GameRoutingModule } from './game-routing.module';
import { NewGameComponent } from './components/new-game/new-game.component';
import { JoinGameComponent } from './components/join-game/join-game.component';



@NgModule({
  declarations: [
    DashboardComponent,
    GameComponent,
    NewGameComponent,
    JoinGameComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    GameRoutingModule,
    NgxsModule.forFeature(
      [UserState]
    ),
    NgxsStoragePluginModule.forRoot(
      {key: UserState}
    ),
    NgxsLoggerPluginModule.forRoot()
  ],
})
export class GameModule { }
