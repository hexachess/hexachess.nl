import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from 'src/app/core/services/game.service';
import { Game } from 'src/app/shared/model/game';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.scss'],
})
export class NewGameComponent implements OnInit {
  constructor(private gameService: GameService, private router: Router) {}

  ngOnInit(): void {}

  newGame(mode: number) {
    this.gameService
      .createGame(new Game(0, mode, 0, 0, [], [], ''))
      .subscribe((response) => {
        this.router.navigate(['/game/id/' + response.id]);
      });
  }
}
