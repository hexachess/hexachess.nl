import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { ToastrService } from 'ngx-toastr';
import { GameService } from 'src/app/core/services/game.service';

@Component({
  selector: 'app-join-game',
  templateUrl: './join-game.component.html',
  styleUrls: ['./join-game.component.scss'],
})
export class JoinGameComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private gameService: GameService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.joinGame();
  }

  private joinGame() {
    let gameId = 0;   
    this.route.params.subscribe((params) => {
      gameId = params.id;
    })
    this.gameService.joinGame(gameId).subscribe((result) => {
      this.router.navigate(['/game/id/' + gameId]);
    }, (error) => {
      this.toastr.error('Failed to join game');
      this.router.navigate(['/game/id/' + gameId]);
    });
  }
}
