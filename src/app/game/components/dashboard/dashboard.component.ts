import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { first } from 'rxjs/operators';
import { EventService } from 'src/app/core/services/event.service';
import { GameService } from 'src/app/core/services/game.service';
import { Game } from 'src/app/shared/model/game';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  private username: string | undefined;
  gameList: Game[] | undefined;

  constructor(
    private route: ActivatedRoute,
    private store: Store,
    private gameService: GameService
  ) {}

  ngOnInit(): void {
    this.getUsername();
    this.getGames();
  }

  private getUsername() {
    this.route.params.subscribe((params) => {
      this.username = params.username;
    });
    if (this.username == undefined) {
      this.store
        .select((state) => state.users.users)
        .pipe(first())
        .subscribe((result) => {
          if (result != undefined) {
            if (result[0] != undefined) {
              this.username = result[0].username;
            }
          }
        }, console.log);
    }
    console.log(this.username);
  }

  private getGames() {
    if (this.username != undefined) {
      this.gameService.getGames(this.username).subscribe(
        (data) => {
          console.log('data');
          console.log(data);
          this.gameList = data;
        },
        (err) => {
          console.log(err);
        }
      );
    }
  }

  remove(id: number){
    this.gameService.removeGame(id).subscribe(()=>
    this.getGames(), ()=>
    this.getGames());
  }
}
