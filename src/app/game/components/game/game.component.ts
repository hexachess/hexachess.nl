import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { EventService } from 'src/app/core/services/event.service';
import { GameService } from 'src/app/core/services/game.service';
import { Game } from 'src/app/shared/model/game';
import { Move } from 'src/app/shared/model/move';
import { User } from 'src/app/shared/model/user';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit {
  private gameId!: number;
  game!: Game;
  oldX!: number;
  oldY!: number;
  newX!: number;
  newY!: number;

  joinEventService: any;
  moveEventService: any;

  constructor(
    private route: ActivatedRoute,
    private eventService: EventService,
    private gameService: GameService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.getGameId();
    this.getGame();
    this.listenForGameUpdates();
  }

  private getGameId() {
    this.route.params.subscribe((params) => {
      this.gameId = params.id;
    });
  }

  private getGame() {
    this.gameService.getGame(this.gameId).subscribe((result) => {
      this.game = result;
      console.log(this.game);
    });
  }

  onSubmit(moveForm: any) {
    this.gameService
      .placeMove(
        new Move(
          new User(''),
          this.game,
          this.oldX,
          this.oldY,
          this.newX,
          this.newY
        )
      )
      .subscribe(
        (result: HttpResponse<Game>) => {
          if (result.body != null) {
            this.game = result.body;
            moveForm.reset();
          }
        },
        (error: HttpErrorResponse) => {
          if (error.status == 400) {
            this.toastr.error('Unable to place move at this time');
          }
        }
      );
  }

  public listenForGameUpdates() {
    this.joinEventService = this.eventService.getGameMoveUpdates(this.gameId).subscribe((message) => {
      this.game.moves.push(JSON.parse(message));
    });
    this.moveEventService = this.eventService.getGameJoinUpdates(this.gameId).subscribe(() => {
      this.getGame();
    });
  }
  
  ngOnDestroy(): void{
    this.joinEventService.unsubscribe();
    this.moveEventService.unsubscribe();
  }
}
